#-------------------------------------------------
#
# Project created by QtCreator 2015-12-21T21:19:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qjamendo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    src/api.cpp \
    src/profile.cpp \
    src/core.cpp \
    src/tools.cpp \
    src/usertools.cpp \
    src/origin.cpp \
    src/localitem.cpp \
    src/tag.cpp \
    src/item.cpp \
    src/radio.cpp \
    src/music.cpp \
    src/album.cpp \
    src/artist.cpp \
    src/playlist.cpp \
    src/remotefile.cpp \
    src/localfile.cpp \
    src/librairy.cpp \
    src/comment.cpp \
    src/user.cpp \
    src/timestamp.cpp

HEADERS  += mainwindow.h \
    include/api.h \
    include/profile.h \
    include/core.h \
    include/tools.h \
    include/usertools.h \
    include/origin.h \
    include/localitem.h \
    include/tag.h \
    include/item.h \
    include/radio.h \
    include/music.h \
    include/album.h \
    include/artist.h \
    include/playlist.h \
    include/remotefile.h \
    include/localfile.h \
    include/librairy.h \
    include/comment.h \
    include/user.h \
    include/timestamp.h

FORMS    += mainwindow.ui
