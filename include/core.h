#ifndef CORE_H
#define CORE_H
#include "artist.h"
#include "album.h"
#include "librairy.h"

class Core
{
public:
    Core();
    void Init();
    void LoadProfile();
    void LoadDatabase();

private:
    Librairy m_librairy;
    Artist* m_artists;
    Album* m_albums;
};

#endif // CORE_H
