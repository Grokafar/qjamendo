#ifndef LIBRAIRY_H
#define LIBRAIRY_H
#include "music.h"
#include "album.h"
#include "artist.h"
#include "playlist.h"
#include "tag.h"
#include "radio.h"

class Librairy
{
public:
    Librairy();
    void Save();
    void Load();
    void Refresh();

private:
    Music* m_musics;
    Album* m_albums;
    Artist* m_artists;
    Playlist* m_playlist;
    Tag* m_tags;
    Radio* m_radio;
};

#endif // LIBRAIRY_H
