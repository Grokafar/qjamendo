#ifndef ALBUM_H
#define ALBUM_H
#include "item.h"
#include "artist.h"
#include "music.h"

class Music;
class Artist;

class Album : public Item
{
public:
    Album();
    void Delete();

private:
    Artist *m_artist;
    Music **m_musics;
};

#endif // ALBUM_H
