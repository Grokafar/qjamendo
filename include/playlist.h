#ifndef PLAYLIST_H
#define PLAYLIST_H
#include "item.h"
#include "music.h"


class Playlist : public Item
{
public:
    Playlist();
    void Delete();

private:
    Music** m_musics;
};

#endif // PLAYLIST_H
