#ifndef MUSIC_H
#define MUSIC_H
#include "item.h"
#include "artist.h"
#include "album.h"
#include "remotefile.h"
#include "localfile.h"

class Album;
class Artist;

class Music : public Item
{
public:
    Music();
    void Delete();

private:
    Album* m_album;
    Artist* m_artist;
    LocalFile m_localFile;
    RemoteFile m_remoteFile;
};

#endif // MUSIC_H
