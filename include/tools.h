#ifndef TOOLS_H
#define TOOLS_H


class Tools
{
public:
    Tools();
    void FindAlbums();
    void FindArtists();
    void ScanLocal();
};

#endif // TOOLS_H
