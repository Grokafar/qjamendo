#ifndef COMMENT_H
#define COMMENT_H
#include <string>
#include "user.h"
#include "timestamp.h"

class Comment
{
public:
    Comment();
    User m_user;
    std::string m_content;
    Timestamp m_timestamp;
};

#endif // COMMENT_H
