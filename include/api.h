#ifndef API_H
#define API_H
#include "profile.h"


class Api
{
public:
    void Connect();
    bool IsProfileSet(Profile *profile);
    Api();
};

#endif // API_H
