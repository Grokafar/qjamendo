#ifndef USERTOOLS_H
#define USERTOOLS_H


class UserTools
{
public:
    void AddMusic();
    void AddAlbum();
    void AddArtist();
    void AddPlaylist();
    void AddTag();

    void DeleteArtist();
    void DeleteAlbum();
    void DeleteMusic();
    void DeletePlaylist();
    void DeleteTag();

    UserTools();
};

#endif // USERTOOLS_H
