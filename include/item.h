#ifndef ITEM_H
#define ITEM_H
#include "origin.h"
#include "comment.h"
#include "tag.h"
#include <string.h>

class Item
{
public:
    Item();
    virtual void Delete() = 0;

private:
    Origin* m_origin();
    unsigned int m_id;
    bool m_favorite;
    Tag* m_tags;
    Comment* m_comments;
    std::string m_name;
};

#endif // ITEM_H
