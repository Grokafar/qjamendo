#ifndef PROFILE_H
#define PROFILE_H
#include <string>


class Profile
{
public:

    Profile();
    bool IsConnected();
    bool Save();
private:
    std::string m_username;
    std::string m_email;
    std::string m_password;
    bool m_isConnected;
};

#endif // PROFILE_H
