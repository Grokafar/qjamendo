#ifndef ARTIST_H
#define ARTIST_H
#include "item.h"
#include "album.h"
#include "music.h"

class Album;
class Music;

class Artist : public Item
{
public:
    Artist();
    void Delete();

private:
    Album** m_albums;
    Music** m_musics;
};

#endif // ARTIST_H
